Sass used as css preprocessor

Used ngx-progressbar https://www.npmjs.com/package/ngx-progressbar to show on top the progress of http requests

Environment variables to maintain the link to api

Used ngx-translate to translation, https://www.npmjs.com/package/@ngx-translate/core and https://www.npmjs.com/package/@ngx-translate/http-loader

Used ngx-bootstrap https://valor-software.com/ngx-bootstrap/​

Used reactive forms instead template forms

Double click on the table item or on the id link to enter edit mode

Change language. In the first time the app will get the language from browser. Default language: en. Available: pt and en.

Routine to put the focus on final of name input. The html attribute ​autofocus works just on first time.

Used moment.js to show date / hour as per timezone of the browser.​

Form created dynamically
