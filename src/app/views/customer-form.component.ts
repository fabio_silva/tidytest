import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

import { Subscription } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';

import { Customer } from './../models/customer.model';
import { CustomerService } from './../services/customer.service';
import { TidyTestService } from '../services/tidy-test.service';

import $ from 'jquery/dist/jquery';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html'
})
export class CustomerFormComponent implements OnDestroy, OnInit {

  private subscription: Subscription;
  private subscriptionLanguage: Subscription;
  deleteMode: boolean;
  fields: Array<any> = this.service.fields;
  form: FormGroup;
  formSubmitted: boolean;
  id: number;
  isNew = true;
  message: any;
  processing: boolean;
  title: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private service: CustomerService,
    private tidyService: TidyTestService,
    private translateService: TranslateService) { }

  ngOnInit() {
    setTimeout(() => {
      this.tidyService.focus($('#inputName'));
    }, 300);

    this.form = this.formBuilder.group(this.service.formGroup);
    /*
    this.form = this.formBuilder.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      phone: [null, [Validators.required]],
      address: [null, [Validators.required]],
      city: [null, [Validators.required]],
      state: [null, [Validators.required, Validators.minLength(2)]],
      zipcode: [null, [Validators.required]]
    });
    */

    this.subscription = this.route.params.subscribe(
      (params: any) => {
        this.id = params['id'];
        this.isNew = (this.id === undefined);

        if (!this.isNew) {
          this.processing = true;
          this.service.get(this.id).subscribe(
            (data: Customer) => {
              if (data) {
                this.processing = false;
                this.form.patchValue(data);
              }
            },
            err => this.setMessage(err)
          );
        }

        this.setTitle();
      }
    );

    this.subscriptionLanguage = this.tidyService.langChange.subscribe(() => this.setTitle());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscriptionLanguage.unsubscribe();
  }

  applyCss(nameField: string) {
    const control: FormControl = this.form.get(nameField) as FormControl;
    return { 'has-error': (this.formSubmitted && !control.valid && control.dirty) };
  }

  delete() {
    if (this.deleteMode) {
      this.processing = true;
      this.service.delete(this.id).subscribe(
        data => {
          this.setMessage('Record_Success_Deleted', 'success');
          setTimeout(() => this.navigateBack(), 2000);
        }
      );
    } else {
      this.deleteMode = true;
    }
  }

  navigateBack() {
    this.router.navigate(['/customers']);
  }

  setMessage(message: string, type: string = 'danger') {
    this.translateService.get(message).subscribe(
      translation => {
        const dismissible: boolean = (['warning', 'danger'].indexOf(type) > -1);
        this.message = { message: translation, type: type, dismissible: dismissible };
      }
    );
  }

  setTitle() {
    let title = '';
    this.translateService.get('Customer')
      .subscribe(
        tranlation => {
          title += tranlation + ' - ';
          this.translateService.get((this.isNew ? 'New' : 'Edit')).subscribe(
            type => {
              title += type;
              if (!this.isNew) {
                title += ' #' + this.id;
              }
              this.title = title;
            }
          );
        }
      );
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.form.valid) {
      this.processing = true;
      const customer: Customer = (this.form.value as Customer);
      customer.id = this.id;
      this.service.save(customer).subscribe(
        data => {
          this.setMessage('Record_Success_' + (this.id ? 'Updated' : 'Created'), 'success');
          setTimeout(() => this.navigateBack(), 2000);
        }
      );
    } else {
      Object.keys(this.form.controls).forEach(
        nameField => {
          const control: FormControl = this.form.get(nameField) as FormControl;
          control.markAsDirty();
        }
      );
      this.setMessage('Invalid_All_Required');
    }
  }
}
