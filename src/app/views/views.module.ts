import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { CustomerFormComponent } from './customer-form.component';
import { CustomerListComponent } from './customer-list.component';
import { CustomerService } from '../services/customer.service';
import { TidyTestService } from './../services/tidy-test.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
    TranslateModule
  ],
  declarations: [
    CustomerFormComponent,
    CustomerListComponent
  ],
  providers: [
    CustomerService,
    TidyTestService
  ]
})
export class ViewsModule { }
