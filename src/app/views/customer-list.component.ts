import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs/Rx';

import { CustomerService } from './../services/customer.service';
import { TidyTestService } from '../services/tidy-test.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styles: [`
    #btnNew {
      margin-bottom: 5px;
    }
  `]
})
export class CustomerListComponent implements OnDestroy, OnInit {
  itens: any = [];
  dateHourFormat: string;
  private subscriptionLanguage: Subscription;

  constructor(
    private router: Router,
    private service: CustomerService,
    private tidyService: TidyTestService) { }

  ngOnInit() {
    this.getAll();
    this.setDateHourFormat();
    this.subscriptionLanguage = this.tidyService.langChange.subscribe(() => this.setDateHourFormat());
  }

  ngOnDestroy() {
    this.subscriptionLanguage.unsubscribe();
  }

  edit(id: number) {
    this.router.navigate([`/customer/${id}`]);
  }

  getAll() {
    this.service.getAll(true).subscribe( data => this.itens = (data ? data : []) );
  }

  new() {
    this.router.navigate(['/customer/']);
  }

  setDateHourFormat() {
    if (this.tidyService.lang === 'en') {
      this.dateHourFormat = 'MM/dd/yyyy HH:mm';
    } else {
      this.dateHourFormat = 'dd/MM/yyyy HH:mm';
    }
  }
}
