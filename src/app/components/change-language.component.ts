import { Component } from '@angular/core';

import { TidyTestService } from '../services/tidy-test.service';

@Component({
  selector: 'app-change-language',
  template: `
  <ul class="nav navbar-nav">
    <li class="dropdown dropdown-menu-left">
      <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <img class="flag" src="./assets/i18n/{{ lang }}.svg" />
        {{ 'language_' + lang | translate }} <span class="caret"></span>
      </a>
      <ul class="dropdown-menu">
        <li *ngFor="let option of LANGS" (click)="changeLang(option)" [class.active]="option === lang">
          <a><img class="flag" (class.active)="option === lang" src="./assets/i18n/{{ option }}.svg" />
          {{ 'language_' + option | translate }}</a>
        </li>
      </ul>
    </li>
  </ul>
  `,
  styles: [`
    .flag {
      width: 16px;
      height: 16px;
      display: inline-block;
      margin-top: -3px;
    }
    ul.dropdown-menu {
      width: -moz-calc(100% + 15px);
      width: -webkit-calc(100% + 15px);
      width: -o-calc(100% + 15px);
      width: calc(100% + 15px);
      margin-top: -5px;
      min-width: auto;
    }
    .navbar-nav > li > a {
      padding-top: 10px;
    }
  `]
})
export class ChangeLanguageComponent {

  LANGS: Array<string> = TidyTestService.LANGS;
  lang: string = this.service.lang;

  constructor(private service: TidyTestService) {
  }

  changeLang(lang: string) {
    this.lang = lang;
    this.service.changeLang(lang);
  }
}
