import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { ChangeLanguageComponent } from './change-language.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    ChangeLanguageComponent
  ],
  exports: [
    ChangeLanguageComponent
  ]
})
export class ComponentsModule { }
