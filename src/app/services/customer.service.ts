import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs/Subject';

import { Customer } from './../models/customer.model';
import { TidyTestService } from './tidy-test.service';

import * as moment from 'moment-timezone';

@Injectable()
export class CustomerService {

  fields: Array<any> = [ { name: 'name' }, { name: 'email' },
    { name: 'phone' }, { name: 'address' },
    { name: 'city' }, { name: 'state' }, { name: 'zipcode' }
  ];
  formGroup: any = {};

  private customers: Array<Customer> = null;
  private customersSubject: Subject<Array<Customer>> = new BehaviorSubject<Array<Customer>>(null);

  constructor(private http: HttpClient,
              private tidyService: TidyTestService) {
    this.fields.map(item => {
      item.type = 'text';
      item.translate = item.name.charAt(0).toUpperCase() + item.name.slice(1);
      if (item.name === 'zipcode') {
        item.translate = 'ZipCode';
      } else if (item.name === 'email') {
        item.type = 'email';
      }
      item.inputName = `input${item.translate}`;

      this.formGroup[item.name] = [null, [Validators.required]];
      if (item.name === 'email') {
        this.formGroup[item.name][1].push(Validators.email);
      }
      if (item.name === 'state') {
        this.formGroup[item.name][1].push(Validators.minLength(2));
      }
    });
  }

  delete(id: number) {
    return this.http.delete(`${environment.apiLink}/${id}`);
  }

  get(id: number): Subject<Customer> {
    const customerSubject: Subject<Customer> = new BehaviorSubject<Customer>(null);

    this.getAll().subscribe((data: Array<Customer>) => {
      if (data) {
        const customer: Customer = data.find((item: Customer) => item.id == id);
        if (customer) {
          customerSubject.next(Object.assign({}, customer));
        } else {
          customerSubject.error('Record_Not_Found');
        }
      }
    });

    return customerSubject;
  }

  getAll(refresh: boolean = false): Subject<Array<Customer>> {
    if (refresh) { this.customers = null; }

    if (this.customers) {
      this.customersSubject.next(this.customers);
    } else {
      this.http.get<Array<Customer>>(environment.apiLink).subscribe(
        data => {
          this.customers = Object.assign(new Array<Customer>(), data);
          this.customers.map( (item: Customer) => {
            item.created_at = moment(item.created_at).tz(this.tidyService.timezone);
            item.updated_at = moment(item.updated_at).tz(this.tidyService.timezone);
          });
          this.customersSubject.next(this.customers);
        },
        err => this.customersSubject.error(err)
      );
    }

    return this.customersSubject;
  }

  save(customer: Customer) {
    if (customer.id) {
      return this.http.put(`${environment.apiLink}/${customer.id}`, customer);
    } else {
      return this.http.post(environment.apiLink, customer);
    }
  }
}
