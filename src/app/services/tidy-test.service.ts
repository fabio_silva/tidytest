import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs/Subject';

import $ from 'jquery/dist/jquery';
import * as moment from 'moment-timezone';

@Injectable()
export class TidyTestService {

  static readonly DEFAULT_LANG = 'en';
  static readonly LANGS: Array<string> = ['pt', 'en'];
  timezone = moment.tz.guess();

  langChange = new Subject<string>();
  private _lang: string;

  get lang(): string {
    return this._lang;
  }

  localStorage = new function() {
    this.JSONtoString = function(object: any) {
        return JSON.stringify(object);
    };

    this.save = function(name: string, data: any){
        localStorage.setItem(name, this.JSONtoString(data));
    };

    this.get = function(name: string) {
        const item = localStorage.getItem(name);
        return JSON.parse(item);
    };

    this.remove = function(name: string) {
        localStorage.removeItem(name);
    };
  };

  constructor(private translateService: TranslateService) {
  }

  changeLang(lang: string, saveLocal: boolean = true) {
    this.translateService.use(lang);
    if (this._lang !== lang) {
      this._lang = lang;
      this.langChange.next(lang);

      if (saveLocal) {
        this.localStorage.save('lang', lang);
      }
    }
  }

  config() {
    this.translateService.addLangs(TidyTestService.LANGS);
    this.translateService.setDefaultLang(TidyTestService.DEFAULT_LANG);

    let lang: string = this.localStorage.get('lang');
    let saveLocal: boolean;
    if (!lang) {
      const browserLang: string = this.translateService.getBrowserLang();
      lang = browserLang.match(/pt|en/) ? browserLang : TidyTestService.DEFAULT_LANG;
      saveLocal = true;
    }

    this.changeLang(lang, saveLocal);
  }

  cursorToEnd(input) {
    input.focus();

    // If this function exists...
    if (input.setSelectionRange) {
        // ... then use it (Doesn't work in IE)
        // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
        const len = $(input).val().length * 2;

        input.setSelectionRange(len, len);
    } else {
        // ... otherwise replace the contents with itself
        // (Doesn't work in Google Chrome)
        $(input).val($(input).val());
    }

    // Scroll to the bottom, in case we're in a tall textarea
    // (Necessary for Firefox and Google Chrome)
    input.scrollTop = 999999;
  }

  focus(jQueryObject) {
    setTimeout(() => this.cursorToEnd(jQueryObject), 300);
  }
}
