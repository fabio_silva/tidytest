import { TestBed, inject } from '@angular/core/testing';

import { TidyTestService } from './tidy-test.service';

describe('TidyTestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TidyTestService]
    });
  });

  it('should be created', inject([TidyTestService], (service: TidyTestService) => {
    expect(service).toBeTruthy();
  }));
});
