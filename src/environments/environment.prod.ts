export const environment = {
  production: true,
  apiLink: 'http://tidy-api-test.herokuapp.com:80/api/v1/customer_data',
  apiLinkDocs: 'http://tidy-api-test.herokuapp.com/apidocs#!/customer_data'
};
